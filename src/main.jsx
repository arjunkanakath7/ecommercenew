import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Root from './routes/root';
import ErrorPage from './error-page';
import Home,{loader as homeloader} from './routes/home';
import Categories,{loader as categoryloader} from './routes/categories';
import Category from './routes/category';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage/>,
    children: [
      {
        path: "/",
        element: <Home/>,
        loader: homeloader
      },
      {
        path: "/categories",
        element: <Categories/>,
        loader: categoryloader
      },
      {
        path: "/categories/:categoryID",
        element: <Category/>,
      }
    ]
  }, 
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
   <RouterProvider router={router} />
  </React.StrictMode>,
)
