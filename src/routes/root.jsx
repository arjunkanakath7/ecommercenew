import React from 'react';
import { Link, Outlet } from 'react-router-dom';

function Root(props) {
    return (
        <>
<header>
        <div className="container">
    <div className="headerleft">
      <img className="logo" src="https://marketplace.canva.com/EAFvDRwEHHg/1/0/1600w/canva-colorful-abstract-online-shop-free-logo-cpI8ixEpis8.jpg" alt="logo" />
      <button className="category">
        <span className="material-symbols-outlined">grid_view</span>
        <span>Category</span>
      </button>
    </div>
    <div className="headercentre">
      <ul className="centre">
         <li>
            <Link id='mainlinks' to={'/'}>Home</Link>
         </li>
         <li>
         <Link id='mainlinks' to={'/about'}>About</Link>
         </li>
         <li>
         <Link id='mainlinks' to={'/categories'}>Categories</Link>
         </li>
         <li>
         <Link id='mainlinks' to={'/contact'}>Contact</Link>
         </li>
      </ul>
    
    </div>
    <div>
      <form className="searche" action="/search" method="get">
        <input type="text" placeholder="Search..." />
        <input type="submit" defaultValue="Search" />
      </form>
      <span id="menus" className="material-symbols-outlined">
        menu
      </span>
    </div>
  </div>
  </header>
  <Outlet/>
  <footer />
</>
    );
}

export default Root;