import React from 'react';
import { useLoaderData } from 'react-router-dom';

export async function loader() {
    const response = await fetch('http://localhost:3001/categories')
    const categories = await response.json()
    return { categories };
  }

function Home(props) {
    const {categories} = useLoaderData()
    console.log(categories)
    return (
        <main className='categories'>
            <h1>Shop by Category</h1>
            <ul>
                {
                    categories.map(category => {
                        return(
                            <li key={category._id}>{category.name}</li>      
                        )
                    })
                }
            </ul>
        </main>
    );
}

export default Home;