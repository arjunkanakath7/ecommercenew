import React from 'react';
import { Link, useLoaderData } from 'react-router-dom';

export async function loader() {
    const response = await fetch('http://localhost:3001/categories')
    const categories  =await response.json()
    return { categories };
  }

function Categories(props) {
    const {categories} = useLoaderData() 
    return (
        <main>
            <section className='containers'>
                <h1>Categories</h1>
                <ul className='categorygrid'>
                    {
                        categories.map(category=>{
                            return(
                                <li key={category._id}>
                                 <Link to={'/categories/${category._id}'}><img src={category.image} alt="" /></Link>   
                                
                                <h2>{category.name}</h2>
                            </li>
                            )
                        })
                    }
                   
                   
                </ul>
            </section>
        </main>
    );
}

export default Categories;